# Armeria

## Saisie et traitement de données botaniques et phytosociologiques pour Excel 

Aremeria se compose  d'un script Excel permettant de faire de la saisie et de l'extraction de données selon les principaux 
référentiels floritiques et pytosociologiques nationaux. Ces référentiels sont empaquetés avec le script et peuvent être mis à jour 
séparemment. 

Le script propose également une série d'outils de création de tableaux phytosociologiques (fusion de tablaeux, création de colonnes 
synthétiques, etc.).